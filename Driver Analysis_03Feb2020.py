# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.3.0
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# ## Netcare Driver Analysis
# ----

# +
import pandas as pd 
import numpy as np 
import matplotlib.pyplot as plt
import seaborn as sns

# %matplotlib inline
# -

# #### Import and clean the data

df = pd.read_excel('C:\\Users\\user\\Desktop\\Netcare\\RawData_03Feb2020.xlsx')

df.head()

# +
# df.describe()
# df.info()
# list(df.columns) 
# -

df_nurse = df[['CaseNumber',
 'DischargeDate',
 'Survey Completion Date',
 'The nurses greeted you',
 'Upon greeting, you were addressed as Mr, Mrs, Sir or Ma’am',
 'The nurses asked your permission before performing any activities that involved you',
 'The nurses explained procedures in a way you could understand',
 'The nurses thanked you after they performed any activities that involved you',
 'The nurses asked you if you needed anything to improve your comfort',
 'The nurses listened to your concerns, worries and discomforts',
 'The nurses responded with care to your emotional and physical needs',
 'Please rate your overall level of satisfaction with the nursing staff']]

df_nurse.head()

df_nurse.columns = ['CaseNumber',
'DischargeDate',
'Survey Completion Date',
'Greet',
'Addressed',
'Permission before touch',
'Explained',
'Thanked after performed',
'Comfort',
'Listened',
'Care',
'Overall Satisfaction']

# +
#df_nurse.head()
# -

df_nurse_clean = df_nurse[df_nurse['Survey Completion Date'] != 'No Survey Completed'].copy()

df_nurse_clean['Survey Completion Date'] = df_nurse_clean['Survey Completion Date'].astype("datetime64")
df_nurse_clean = df_nurse_clean.apply(lambda x: x.str.strip() if x.dtype == "object" else x) 

# +
scale1 = {"Always": 10, 
          "Usually": 10, 
          "Sometimes": 0, 
          "Never": 0, 
          "I don’t remember": np.nan, 
          "Not applicable":np.nan }


scale2 = {"Excellent": 10, 
          "Very Good": 10, 
          "Good": 10, 
          "Fair": 5, 
          "Poor": 0, 
          "Not applicable": np.nan}
# -

df_nurse_clean.replace(scale1, inplace=True)
df_nurse_clean.replace(scale2, inplace=True)

pd.options.display.float_format = '{:.5f}'.format
round(df_nurse_clean.mean(),1)

df_nurse_clean[['Overall Satisfaction','Greet','Addressed','Permission before touch','Explained',
                'Thanked after performed','Comfort','Listened','Care','Overall Satisfaction']].count()

df_nurse_clean['Overall Satisfaction'] = df_nurse_clean['Overall Satisfaction'].astype("float64")
df_nurse_clean['date'] = df_nurse_clean['DischargeDate'].dt.strftime('%Y-%m')

df_nurse_clean[['date','Overall Satisfaction']].groupby(['date']).mean()

df_nurse_clean[['date','Overall Satisfaction']].groupby(['date']).count()

df_nurse_clean1 = df_nurse_clean.dropna()

df_nurse_clean1.head()

# #### Data Exploration

ax = sns.heatmap(df_nurse_clean.corr(),annot=True, cmap="PuBu", linewidths=0.5)
ax.figure.set_size_inches(18.5, 10.5)

# #### Regression Analysis
# ----

# +
X = df_nurse_clean1[['Greet',
'Addressed',
'Permission before touch',
'Explained',
'Thanked after performed',
'Comfort',
'Listened',
'Care']]

y = df_nurse_clean1['Overall Satisfaction']
# -

from sklearn.linear_model import LinearRegression

lm = LinearRegression()

lm.fit(X,y)

lm.coef_

coeff = (0.24768313,  0.04880237,  0.06572944,  0.16931354, -0.01037209,
       -0.01623063, -0.04584241,  0.30373387)

Xcolumns = ('Greet',
'Addressed',
'Permission before touch',
'Explained',
'Thanked after performed',
'Comfort',
'Listened',
'Care')

coeff_df1 = pd.DataFrame(coeff,Xcolumns,columns=['Coefficient'])
coeff_df1
